<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    @vite('resources/scss/app.scss')
</head>

<body>
    @include('home._menu')

    <div class="container">
        <div class="row">
            <h1 class="text-center my-4">Estamos en Home</h1>
        </div>
    </div>

    @vite('resources/js/app.js')
</body>

</html>
