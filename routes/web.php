<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

Route::get('/', [HomeController::class, 'index']);
Route::get('/home/index', [HomeController::class, 'index'])->name('index');

Route::get('/home/mensaje', [HomeController::class, 'mensaje'])->name('mensaje');
Route::get('/home/listado', [HomeController::class, 'listado'])->name('listado');
Route::get('/home/imagen', [HomeController::class, 'imagen'])->name('imagen');