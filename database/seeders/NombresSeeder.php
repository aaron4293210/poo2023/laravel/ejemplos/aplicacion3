<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NombresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('nombres')->insert([
            ['nombre' => 'Pepe', 'created_at' => now(), 'updated_at' => now()],
            ['nombre' => 'Juan', 'created_at' => now(), 'updated_at' => now()],
            ['nombre' => 'Maria', 'created_at' => now(), 'updated_at' => now()],
            ['nombre' => 'Pedro', 'created_at' => now(), 'updated_at' => now()],
        ]);
    }
}
