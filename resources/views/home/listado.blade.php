<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Listado</title>
    @vite('resources/scss/app.scss')
</head>

<body>
    @include('home._menu')

    <div class="container">
        <div class="row">
            <h1 class="text-center my-4">Estamos en Listado</h1>
            
            @foreach ($nombres as $nombre)
                <div class="card-group col-md-4 mb-4 text-center">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title font-italic text-primary">NOMBRES</h4>
                        </div>

                        <div class="card-body text-center">
                            <p class="card-text lead text-muted">{{ $nombre }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    @vite('resources/js/app.js')
</body>

</html>