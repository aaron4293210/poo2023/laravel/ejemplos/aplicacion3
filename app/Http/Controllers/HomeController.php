<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index()
    {

        return view('home.index');
    }

    public function mensaje()
    {
        $mensaje = "Esto es un mensaje";

        return view('home.mensaje', ['mensaje' => $mensaje]);
    }

    public function listado()
    {
        $nombres = ["Pepe", "Pepa", "Pepito", "Pepete"];

        // $datos = DB::table('sessions')->get();

        return view('home.listado', ['nombres' => $nombres]);
    }

    public function imagen()
    {
        $mensaje = "Esto es un mensaje";

        return view('home.imagen');
    }
}
